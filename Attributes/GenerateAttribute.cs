#region Disclaimer
// <copyright file="GenerateAttribute.cs">
// Copyright (c) 2019 - 2019 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion

namespace Shygen.Runtime.Attributes
{
    using System;

    /// <summary>
    /// Place on file so Shygen knows that it should generate code for this file
    /// </summary>
    // TODO: Mark as abstract once we have migrated all properties to inherited types
    public class GenerateAttribute : Attribute
    {
        /// <summary>
        /// The module this file belongs to. (e.g. Input, Movement, etc.)
        /// </summary>
        public string ModuleName { get; private set; }
        
        /// <summary>
        /// Pass the attribute type of the category which this type belongs to. (e.g. Model, System, etc.)
        /// </summary>
        public Type CategoryAttributeType { get; private set; }

        public GenerateAttribute(string moduleName, Type categoryAttributeType)
        {
            ModuleName = moduleName;
            CategoryAttributeType = categoryAttributeType;
        }
    }
}