#region Disclaimer
// <copyright file="GenerateMemberAttribute.cs">
// Copyright (c) 2019 - 2019 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion

namespace Shygen.Runtime.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Property)]
    public class GenerateMemberAttribute : Attribute
    {
    }
}