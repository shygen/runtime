﻿namespace Shygen.Runtime.Attributes
{
    using System;

    public class GenerationAttributeTypeAttribute : Attribute
    {
        public Type AttributeType { get; set; }

        public GenerationAttributeTypeAttribute(Type attributeType)
        {
            AttributeType = attributeType;
        }
    }
}
