namespace Shygen.Runtime.Attributes
{
    public struct TemplateConfiguration
    {
        public const string FrontendScope = "frontendScope";
        public const string FrontendEditorScope = "frontendEditorScope";
        public const string BackendScope = "backendScope";
        public const string SharedScope = "sharedScope";
        
        
        public string Path;
        public string Scope;

        public TemplateConfiguration(string path, string scope)
        {
            Path = path;
            Scope = scope;
        }

    }
}