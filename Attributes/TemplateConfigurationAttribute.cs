﻿using RobinBird.Utilities.Runtime.Extensions;
using Shygen.Runtime.CSharpGeneration;

namespace Shygen.Runtime.Attributes
{
    using System;

    /// <summary>
    /// Binds a template to a category
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class TemplateConfigurationAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Attribute" /> class.
        /// </summary>
        public TemplateConfigurationAttribute(string scope, Type categoryType, params string[] templatePaths)
        {
	        CategoryType = new TypeData(categoryType);
	        var paths = new TemplateConfiguration[templatePaths.Length];
	        for (int i = 0; i < templatePaths.Length; i++)
	        {
		        var path = templatePaths[i];
		        paths[i] = new TemplateConfiguration(path, scope);
	        }
	        TemplatePaths = paths;
        }


        public TemplateConfigurationAttribute(TypeData categoryType, params string[] templatePaths)
        {
            CategoryType = categoryType;
            var paths = new TemplateConfiguration[templatePaths.Length];
            for (int i = 0; i < templatePaths.Length; i++)
            {
                var path = templatePaths[i];
                paths[i] = new TemplateConfiguration(path, TemplateConfiguration.FrontendScope);
            }
            TemplatePaths = paths;
        }

        public TypeData CategoryType { get; set; }

        public TemplateConfiguration[] TemplatePaths { get; set; }
    }
}