﻿namespace Shygen.Runtime.Attributes
{
    using System;

    /// <summary>
    /// Attribute to mark classes that belong to a certain modules.
    /// Useful to display module specific menus in editor.
    /// </summary>
    public class ShygenModuleAttribute : Attribute
    {
        public ShygenModuleAttribute(string moduleName)
        {
            ModuleName = moduleName;
        }

        public string ModuleName { get; set; }
    }
}