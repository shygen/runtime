﻿#region Disclaimer
// <copyright file="TemplateNameAttribute.cs">
// Copyright (c) 2017 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion
namespace Shygen.Runtime.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class)]
    public class TemplateNameAttribute : Attribute
    {
        public string Name { get; private set; }

        public TemplateNameAttribute(string name)
        {
            Name = name;
        }
    }
}