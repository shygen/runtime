﻿#region Disclaimer
// <copyright file="TemplateExtensionAttribute.cs">
// Copyright (c) 2017 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion
namespace Shygen.Runtime.Attributes
{
    using System;

    public class TemplateExtensionAttribute : Attribute
    {
        public string Extension { get; private set; }

        /// <summary>
        /// Constructor for attribute
        /// </summary>
        /// <param name="extension">The extension for the resulting file through this template. (default: ".cs")</param>
        public TemplateExtensionAttribute(string extension)
        {
            Extension = extension;
        }
    }
}