﻿namespace Shygen.Runtime.Attributes
{
    using System;

    [Obsolete("Will not work with assembly generation anymore. use [Generate] attribute.")]
    public class GroupAttribute : Attribute
    {
        public string GroupName { get; set; }

        public GroupAttribute(string groupName)
        {
            GroupName = groupName;
        }
    }
}
