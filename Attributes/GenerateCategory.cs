﻿namespace Shygen.Runtime.Attributes
{

    public class GenerateCategory
    {
        public string Name { get; set; }

        public GenerateCategory(string name)
        {
            Name = name;
        }
    }
}
