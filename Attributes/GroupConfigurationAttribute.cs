﻿using System.Collections.Generic;

namespace Shygen.Runtime.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class GroupConfigurationAttribute : Attribute
    {
        public GroupConfigurationAttribute(string groupName, string @namespace, params string[] moduleNames)
        {
            GroupName = groupName;
            Namespace = @namespace;
            var tmpModules = new List<string>(moduleNames);
            tmpModules.Sort();
            ModuleNames = tmpModules.ToArray();
        }

        public string GroupName { get; set; }
        public string Namespace { get; set; }
        public string[] ModuleNames { get; set; }
    }
}