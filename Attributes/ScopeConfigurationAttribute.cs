using System;

namespace Shygen.Runtime.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ScopeConfigurationAttribute : Attribute
    {
        public string Group { get; }
        public string Scope { get; }
        public string Path { get; }

        public ScopeConfigurationAttribute(string group, string scope, string path)
        {
            Group = @group;
            Scope = scope;
            Path = path;
        }
    }
}