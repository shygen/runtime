﻿#region Disclaimer
// <copyright file="CSharpGenerationHelper.cs">
// Copyright (c) 2017 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion
namespace Shygen.Runtime.CSharpGeneration
{
    public static class CSharpGenerationHelper
    {
        private static PropertyType[] _defaultPropertyTypes;
        public static readonly PropertyType IntProperty = new PropertyType(typeof (int));
        public static readonly PropertyType FloatProperty = new PropertyType(typeof (float));
        public static readonly PropertyType BoolProperty = new PropertyType(typeof (bool));
        public static readonly PropertyType StringProperty = new PropertyType(typeof (string));

        public static PropertyType[] DefaultPropertyTypes()
        {
            if (_defaultPropertyTypes == null)
            {
                _defaultPropertyTypes = new[]
                {
                    IntProperty, FloatProperty, BoolProperty, StringProperty
                };
            }
            return _defaultPropertyTypes;
        }
    }
}