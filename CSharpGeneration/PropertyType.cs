using System.Reflection;

namespace Shygen.Runtime.CSharpGeneration
{
    using System;
    using System.Collections.Generic;
    using RobinBird.Logging.Runtime;
    using RobinBird.Utilities.Runtime.Extensions;

    /// <summary>
    /// Type of a property that can be used to describe a C# property in code generation
    /// </summary>
    [Serializable]
    public class PropertyType : ICloneable
    {
        private string name;
        private string fullName;

        private bool offerSubType;
        private Type subType;
        private string subTypeNamespace;
        private string subTypeName;
        private List<string> attributeFullNames = new List<string>();
        private List<string> interfaceFullNames = new List<string>();

        /// <summary>
        /// Constructor for managed context with valid type information
        /// </summary>
        public PropertyType(Type type, bool offerSubType = false, Type subType = null)
        {
            if (type.IsGenericType)
            {
                CreateGenericName(type);
            }
            else
            {
                fullName = type.FullName;
                name = type.Name;
                if (type.Name[0] == 'I' && Char.IsUpper(type.Name[1]))
                {
                    // If we are dealing with an interface reduce it to its normal form. Makes generating much easier
                    var newName = type.Name.Remove(0, 1);
                    this.fullName = this.fullName.Replace(name, newName);
                    name = newName;
                }
            }

            foreach (Attribute attribute in type.GetCustomAttributes())
            {
	            attributeFullNames.Add(attribute.GetType().FullName);
            }

            foreach (Type @interface in type.GetInterfaces())
            {
	            interfaceFullNames.Add(@interface.FullName);
            }

            BaseTypeFullName = type.BaseType?.FullName;
            IsEnum = type.IsEnum;
            IsClass = type.IsClass;
            IsStruct = type.IsStruct();
            this.offerSubType = offerSubType;
            SubType = subType;
        }

        /// <summary>
        /// Roslyn constructor for direct information from syntax tree
        /// </summary>
        public PropertyType(string fullName, bool offerSubType = false, Type subType = null)
        {
            var nameSplit = fullName.Split('.');
            this.name = nameSplit[nameSplit.Length - 1];
            this.fullName = fullName;
            if (name[0] == 'I' && Char.IsUpper(name[1]))
            {
                // If we are dealing with an interface reduce it to its normal form. Makes generating much easier
                var newName = name.Remove(0, 1);
                this.fullName = this.fullName.Replace(name, newName);
                name = newName;
            }

            this.offerSubType = offerSubType;
            SubType = subType;

            // We actually don't know at this point
            IsEnum = false;
            IsClass = false;
            IsStruct = false;
        }

        public PropertyType(TypeData type, bool offerSubType = false, Type subType = null)
        {
            var nameSplit = type.FullName.Split('.');
            name = nameSplit[nameSplit.Length - 1];
            fullName = type.FullName;
            this.offerSubType = offerSubType;
            this.subType = subType;

            IsEnum = type.IsEnum;
            IsClass = type.IsClass;
            IsStruct = type.IsStruct;
        }

        public PropertyType()
        {
        }

        public static bool operator == (PropertyType a, Type b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
            {
                return true;
            }

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
            {
                return false;
            }

            return a.FullName == b.Name;
        }

        public static bool operator !=(PropertyType a, Type b)
        {
            return !(a == b);
        }

        public string Name
        {
            get => name;
            set => name = value;
        }

        public string FullName
        {
            get { return fullName; }
            set { fullName = value; }
        }

        public string BaseTypeFullName { get; set; }

        public bool HasAttribute(string attributeFullName)
        {
	        return attributeFullNames.Contains(attributeFullName);
        }

        /// <summary>
        /// Checks against full name
        /// </summary>
        public bool HasInterface(string interfaceFullname)
        {
	        return interfaceFullNames.Contains(interfaceFullname);
        }

        /// <summary>
        /// Uses string.Contains on every interface to find a interface which matches <paramref name="interfaceSearchPhrase"/>
        /// </summary>
        public bool HasInterfaceFuzzy(string interfaceSearchPhrase)
        {
	        foreach (string interfaceFullName in interfaceFullNames)
	        {
		        if (interfaceFullName.Contains(interfaceSearchPhrase))
			        return true;
	        }

	        return false;
        }


#if UNITY_2020_1_OR_NEWER
        [Newtonsoft.Json.JsonIgnore]
#else
	    [System.Text.Json.Serialization.JsonIgnore]
#endif
        public Type SubType
        {
            get
            {
                if (subType == null && string.IsNullOrEmpty(subTypeName) == false)
                {
                    string typeName = subTypeNamespace + "." + subTypeName;
                    subType = AppDomain.CurrentDomain.GetType(typeName);
                    if (subType == null)
                    {
                        Log.Error(string.Format("Could not find sub type for name: {0}", typeName));
                    }
                }
                return subType;
            }
            set
            {
                subType = value;
                subTypeNamespace = subType != null ? subType.Namespace : string.Empty;
                subTypeName = subType != null ? subType.Name : string.Empty;
            }
        }

        public bool OfferSubType
        {
            get { return offerSubType; }
            set { offerSubType = value; }
        }

        public string SubTypeNamespace
        {
            get { return subTypeNamespace; }
            set { subTypeNamespace = value; }
        }

        public string SubTypeName
        {
            get { return subTypeName; }
            set { subTypeName = value; }
        }

        public bool IsEnum { get; set; }
        public bool IsClass { get; set; }

        public bool IsStruct { get; set; }
        public bool IsBool => FullName == typeof(bool).FullName;
        public bool IsInt => FullName == typeof(int).FullName;
        public bool IsString => FullName == typeof(string).FullName;

        public bool IsFloat => FullName == typeof(float).FullName;

        public override string ToString()
        {
            if (OfferSubType)
            {
                return CreateTypeName(SubTypeNamespace, SubTypeName);
            }
            return CreateTypeName(null, FullName);
        }

        private static string CreateTypeName(string ns, string name)
        {
            return string.Format("{0}{2}{1}", ns, name.Replace("+", "."), string.IsNullOrEmpty(ns) ? string.Empty : ".");
        }

        public string ToCSharpType()
        {
            return ToString();
        }

        public string ToCSharpTypeInterface()
        {
            var result = ToString();
            return result.Insert(result.LastIndexOf('.') + 1, "I");
        }

        /// <summary>
        /// Return type name first to view in a list so for example 'IList (System.Generics.Collections)'
        /// </summary>
        public string TypeFirstToString()
        {
            var dotIndex = FullName.LastIndexOf(".", StringComparison.Ordinal);
            if (dotIndex < 0)
            {
                return FullName;
            }

            var @namespace = FullName.Remove(dotIndex);
            var typeName = FullName.Remove(0, dotIndex + 1);
            return $"{@namespace}.{typeName}";
        }

        protected bool Equals(PropertyType other)
        {
            return string.Equals(fullName, other.fullName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((PropertyType) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (fullName != null ? fullName.GetHashCode() : 0);
            }
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        private void CreateGenericName(Type type)
        {
            var genericArguments = type.GenericTypeArguments;

            string genericNamePart = "<";
            for (int i = 0; i < genericArguments.Length; i++)
            {
                var arg = new PropertyType(genericArguments[i]);

                genericNamePart += arg.ToCSharpType();

                if (i < genericArguments.Length - 1)
                {
                    genericNamePart += ", ";
                }
            }

            genericNamePart += ">";
            fullName = type.FullName.Remove(type.FullName.IndexOf("`", StringComparison.Ordinal)) + genericNamePart;
        }
    }
}