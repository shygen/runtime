namespace Shygen.Runtime.CSharpGeneration
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using RobinBird.Utilities.Runtime;
    using RobinBird.Utilities.Runtime.Extensions;

    /// <summary>
    /// A property entry for a <see cref="DataDraft" />. This class
    /// helps generating properties.
    /// </summary>
    [Serializable]
    public class Property
    {
        private PropertyQuantity quantity = PropertyQuantity.Single;

        private PropertyType type;

        private PropertyType additionalType;

        private readonly bool isFiringChanged = true;
        private readonly string attributes;

        public Property()
        {
        }

        public Property(string name, PropertyType type, PropertyQuantity quantity, bool isFiringChanged, string attributes = "")
        {
            Name = name;
            this.type = type;
            this.quantity = quantity;
            this.isFiringChanged = isFiringChanged;
            this.attributes = attributes;
        }


        public Property(PropertyInfo property)
        {
            Type propertyType = property.PropertyType;
            Type elementType = propertyType.GetElementType();
            if (elementType == null)
            {
                elementType = propertyType;
            }

            PropertyQuantity typeQuantity = PropertyQuantity.Single;

            if (propertyType.IsArray)
            {
                typeQuantity = PropertyQuantity.Array;
            }
            else if (propertyType.IsAssignableFrom(typeof(List<>)))
            {
                typeQuantity = PropertyQuantity.List;
            }

            Name = property.Name;
            type = new PropertyType(elementType);
            quantity = typeQuantity;
            IsAlreadyDeclared = true;
        }

        public string Name { get; set; }

        /// <summary>
        /// Describes if this property has been retrieved from source data and is already present.
        /// If true it should not be generated again since it would appear double.
        /// </summary>
        public bool IsAlreadyDeclared { get; set; }

        public PropertyQuantity Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public bool IsFiringChanged => isFiringChanged;
        public string Attributes => attributes;

        public PropertyType Type
        {
            get { return type; }
            set { type = value; }
        }

        /// <summary>
        /// Store additional type for some information. One example for usage
        /// is a property of type Dictionary which requires two types.
        /// </summary>
        public PropertyType AdditionalType
        {
            get
            {
                if (additionalType == null)
                {
                    additionalType = new PropertyType();
                }
                return additionalType;
            }
            set { additionalType = value; }
        }

        /// <summary>
        /// Returns type as compilable string. (e.g. <![CDATA[ List<int>, bool[], bool ]]>
        /// </summary>
        public string ToCSharpType(bool withQuantity = true)
        {
	        string cSharpType = Type.ToCSharpType();
	        return withQuantity ? ApplyQuantityToType(cSharpType) : cSharpType;
        }

        public string ApplyQuantityToType(string cSharpType)
        {
	        string preQuantity = string.Empty;
	        string postQuantity = string.Empty;

	        switch (Quantity)
	        {
		        case PropertyQuantity.Array:
			        postQuantity = "[]";
			        break;
		        case PropertyQuantity.TwoDimensionalArray:
			        postQuantity = "[][]";
			        break;
		        case PropertyQuantity.List:
			        preQuantity = string.Format("{0}<", typeof(List<>).FullNameWithoutGenericParameters());
			        postQuantity = ">";
			        break;
		        case PropertyQuantity.Pool:
			        preQuantity = string.Format("{0}<", typeof(Pool<>).FullNameWithoutGenericParameters());
			        postQuantity = ">";
			        break;
		        case PropertyQuantity.Dictionary:
			        return string.Format("{0}<{1},{2}>", typeof(Dictionary<,>).FullNameWithoutGenericParameters(), cSharpType,
				        AdditionalType.ToCSharpType());
	        }

	        return string.Format("{0}{1}{2}", preQuantity, cSharpType, postQuantity);
        }

        public override string ToString()
        {
            string postfix;
            switch (Quantity)
            {
                case PropertyQuantity.Array:
                    postfix = "[]";
                    break;
                case PropertyQuantity.TwoDimensionalArray:
                    postfix = "[][]";
                    break;
                default:
                    postfix = string.Empty;
                    break;
            }
            return string.Format("{0} ({1}{2})", Name, Type, postfix);
        }
    }
}