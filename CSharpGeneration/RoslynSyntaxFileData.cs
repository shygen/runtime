using System;
using RobinBird.Utilities.Runtime.Extensions;

namespace Shygen.Runtime.CSharpGeneration
{
    public class SyntaxWalkerResult
    {
        public RoslynSyntaxFileData[] Files { get; set; }
    }
    
    public class RoslynSyntaxFileData
    {
        public string Path { get; set; }
        public string ClassName { get; set; }
        
        public string Namespace { get; set; }
        
        public TypeData Type { get; set; }
        
        public AttributeData[] Attributes { get; set; }

        public class AttributeData
        {
            public TypeData Type { get; set; }
            
            public ArgumentData[] Arguments { get; set; }
        }

        public class ArgumentData
        {
            public TypeData Type { get; set; }
            
            public object Value { get; set; }
        }

        public enum ArgumentType
        {
            Default,
            TypeOf,
            Literal,
            Enum
        }
    }

    public class TypeData
    {
        public TypeData(Type type)
        {
            FullName = type.FullName;
            IsEnum = type.IsEnum;
            IsClass = type.IsClass;
            IsStruct = type.IsStruct();

        }

        public TypeData(string fullName, bool isEnum, bool isClass, bool isStruct)
        {
            FullName = fullName;
            IsEnum = isEnum;
            IsClass = isClass;
            IsStruct = isStruct;
        }

        public TypeData()
        {
            
        }
        
        public string FullName { get; set; }
            
        public bool IsEnum { get; set; }
            
        public bool IsClass { get; set; }
        
        public bool IsStruct { get; set; }
            
        public static bool operator == (TypeData typeData, Type type)
        {
            if (ReferenceEquals(typeData, null) && ReferenceEquals(type, null))
            {
                return true;
            }
                
            return typeData?.FullName == type?.FullName;
        }

        public static bool operator !=(TypeData typeData, Type type)
        {
            return !(typeData == type);
        }

        protected bool Equals(TypeData other)
        {
            return FullName == other.FullName && IsEnum == other.IsEnum && IsClass == other.IsClass;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((TypeData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (FullName != null ? FullName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsEnum.GetHashCode();
                hashCode = (hashCode * 397) ^ IsClass.GetHashCode();
                return hashCode;
            }
        }
    }
}