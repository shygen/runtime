﻿namespace Shygen.Runtime.CSharpGeneration
{
    public enum PropertyQuantity
    {
        Single,
        Array,
        TwoDimensionalArray,
        List,
        Pool,
        Dictionary,
    }
}
